import React, { useEffect, useState } from 'react';
import styles from '../css/list.module.css';
import axios from 'axios';
import Sidelist from './sidelist';
// import { BrowserRouter, Route, Routes, Switch, Link } from "react-router-dom";

// import { Card, CardContent, CardMedia, Typography, CardActionArea } from '@material-ui/core';
import { CCard, CRow, CCol, CCardImage, CCardBody, CCardTitle, CCardText, } from '@coreui/react';
import { Button, ButtonToolbar, Row, Col, Card } from 'react-bootstrap';
import Item from './item';
import Detail from './detail';
import { Link } from 'react-router-dom';



const List = (props) => {
    const [resData, setResData] = useState([]);

    const [number, setNumber] = useState(6);


    const apiKey = process.env.REACT_APP_YOUTUBE_API_KEY;
    const baseUrl = process.env.REACT_APP_YOUTUBE_API_URL
    const searchVal = props.searchVal;


    console.log(`props searchVal: ${props.searchVal}`);

    const videoParams = {
        key: apiKey,
        chart: 'mostPopular',
        maxResults: 25,
        regionCode: 'kr',
        part: 'snippet'
    }

    const searchParams = {
        key: apiKey,
        part: 'snippet',
        maxResults: 25,
        q: searchVal
    }

    // let resData = null;
    let listItems = null;
    const test = 'test';

    const aa = [
        {id: 1, comment: 'aa'},
        {id: 2, comment: 'bb'},
        {id: 3, comment: 'cc'},
    ]
    
    let bb = null;

    bb = aa.map( (item, index) => 
        
        <li key={item.id}> {item.comment} </li>,
    );
   


    const getMostPopular = async () => {
        console.log(`getMostPopular()`);
        let params = null;
        let url = null;

        if(searchVal === '') {
            params = videoParams
            url = baseUrl + '/videos'
        } else{
            params = searchParams
            url = baseUrl + '/search'
        }

        console.log(`url : ${url}`);
        console.log(`params : ${JSON.stringify(params)}`);

        await axios.get(url, {params})
            .then(res => {
                console.log(`res : ${res.data.items}`);
                let resData2 = res.data.items;
                let data = res.data.items;

                setResData(data);

                setNumber((number) => number + 1);


                // console.log(`number : ${number}`);
                // console.log(`res : ${res.data.items[0].snippet.thumbnails.default.url}`);
                // console.log(`typeof : ${typeof res.data.items}`);
                // console.log(`resData : ${resData}`);
                // console.log(`resData2 : ${resData2}`);

                resData2.map((datas, i) =>{
                    console.log(`datas : ${datas.id}`);
                });


                listItems = resData2.map((datas, i) =>
                    <li key={datas.id}>{datas.id}</li>
                );

                props.handleYoutubeData(data);
                
                // console.log(`listItems: ${listItems}`);
                

                
            
                // const aa = [1,2,3,4,5, 6]
            
                // bb = aa.map( (item, index) => 
                    
                //     <li key={item.id}> {item.comment} </li>,
                // );

            }).catch( err => {
                console.log(`err : ${err}`);
            })
    }


    useEffect(() => {
        console.log(`useEffect() `);
        getMostPopular();
    }, [props.searchVal])
    // https://i.ytimg.com/vi/00QQWJIFxDA/default.jpg

    return (
        
        <>
            
            <ul className={styles.todo_ul}>
            {/* {listItems} */}
            {/* {bb} */}
            {
                resData.map((x) => 
                
                    <Item
                        key={searchVal === '' ? x.id : x.id.videoId}
                        list={x}/>
                    //   <li key={x.id}> {x.id} </li>
                )
            }
            </ul>
            
        </>
    );
};

export default List;