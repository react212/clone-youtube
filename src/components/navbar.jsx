import React, { useRef, useState } from 'react';
import logo from '../yt_logo.png';
import styles from '../css/navbar.module.css';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';

const Navbar = (props) => {
    const tsIndex = process.env.REACT_APP_YOUTUBE_API_KEY;
    const url = process.env.REACT_APP_YOUTUBE_API_URL;
    const navigate = useNavigate();
    const searchVal = useRef();

    console.log(`process.env: ${process.env}`);
    console.log(`tsIndex : ${tsIndex}`);
    console.log(`url : ${url}`);


    const params = {
        key: process.env.REACT_APP_YOUTUBE_API_KEY,
        chart: 'mostPopular',
        maxResults: 25,
        part: 'snippet'
    };

    const onSubmit = (e) => {
        e.preventDefault();
        console.log(`searchVal : ${searchVal.current.value}`);
        props.onSearchVal(searchVal.current.value);
        navigate('/')
        
    };
    
    return (
        <div className={styles.nav_container}>
            <form onSubmit={onSubmit}>
                <img
                    className={styles.nav_logo}
                    src={require('../yt_logo.png') } alt=""
                    />
                <input
                    className={styles.nav_search_input}
                    type="text" 
                    placeholder='Search..'
                    size='60'
                    ref={searchVal}
                    />

                {/* <span
                    className={styles.nav_search_btn}>dadsafasa</span> */}

                {/* <img 
                    className={styles.nav_search_btn}
                    src={require('../search_icon.png') } alt="" /> */}

                <input type="image" className={styles.nav_search_btn} src={require('../search_icon.png')} />
            </form>
            
        </div>
    );
};

export default Navbar;