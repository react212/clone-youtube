import React from 'react';
import List from './list';
import Navbar from './navbar';

const Main = () => {
    return (
        <div>
            <Navbar/>
            <List/>
        </div>
    );
};

export default Main;