import React, { useRef } from 'react';
import styles from '../css/item.module.css';
import { BrowserRouter, Route, Switch, Link } from "react-router-dom";
import Detail from './detail';

const Item = (props) => {
    // console.log(`props: ${props.list.id}`);
    // console.log(`props: ${props.list.snippet.thumbnails.high.url}`);
    const list = props.list;
    const thumbImg = list.snippet.thumbnails.default.url;
    const kind = list.kind.split('#')[1];

    let videoId = list.id;
    const liWidth = useRef();


    // console.log(`videoId00: ${videoId}`);
    // console.log(`videoId: ${videoId}`);
    if(kind === 'searchResult') {
       videoId = list.id.videoId;
    }

    // console.log(`videoId11: ${videoId}`);

    const videoTitle = list.snippet.title;

    const videosData = {
        videoId,
        videoTitle
    }

    // console.log(`videosData : ${ JSON.stringify(videosData)}`);
    // const imgWidth = list.snippet.thumbnails.default.width;
    // const imgHeigth = list.snippet.thumbnails.default.height;
    const title = list.snippet.title;

    const handleMouseOver = (e) => {
        e.target.style.transform = 'scale(1.3)'
    };

    const onClickEvent = () => {

    };

    return (
        <>
            {/* <li className={ styles.todo_li }>
                <img src="https://i.ytimg.com/vi/gim2kprjL50/hqdefault.jpg" alt="" />
            </li> */}
               
                <li
                    ref={liWidth}
                    // onMouseOver={handleMouseOver}
                    className={ styles.todo_li }
                    // onmouseover={handleMouseOver}
                    // onClick={onClickEvent}
                    >

                    
                    <Link to={`/components/detail/${videoId}`} style={{ textDecoration: 'none', color: 'black' }}>
                        <div className={styles.div_item_container} style={{ position: 'relative', boxShadow: '0 4px 8px 0 rgba(0,0,0,0.2)'}}>
                            <img
                                src={thumbImg} alt="Avatar" style={{ }} />
                            <span className={styles.todo_title} style={{position: 'absolute', }}> {title} </span>
                        </div>
                        {/* <div style={{display: 'inline-block'}}>
                            <span> {title} </span>
                        </div> */}
                        
                            
                        
                    </Link>
                </li>
                
            
            
        </>
        

        // <li>
        //     {list.id}
        // </li>
    );
};

export default Item;