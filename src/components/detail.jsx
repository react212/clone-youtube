import React from 'react';
import { useParams } from 'react-router-dom';
import Item from './item';
import List from './list';
import Sidelist from './sidelist';
import styles from '../css/detail.module.css';

const Detail = (props) => {
    const {videosId} = useParams();
    // console.log(`videosData: ${ JSON.stringify(videosData)}`);
    // console.log(`videoId : ${  JSON.parse(videosData)}`);
    console.log(`props youtubeData : ${props.youtubeData}`);
    let title = null;
    let description = null;
    const youtubeData = props.youtubeData;

    // console.log(`youtubeData.kind : ${JSON.stringify(youtubeData)}`);

    const kind = youtubeData[0].kind.split('#')[1];

    console.log(`kind : ${kind}`);

    const initData = () => {
        youtubeData.filter( item => {
            if(kind === 'searchResult' ? item.id.videoId : item.id === videosId) {
                title = item.snippet.title;
                description = item.snippet.description;
            }
        });

        console.log(`title : ${title}`);
        console.log(`description : ${description}`);
    }

    initData();

    return (
        <>  
        <div style={{
            // display: 'block'
        }}>
            <div style={{
                width: '70%',
                float: 'left'
                
            }}>
                <iframe id="ytplayer" width="100%" height="360" style={{ padding: '1em'}}
                    src={`https://www.youtube.com/embed/${videosId}?autoplay=1&origin=http://localhost:3000`}
                    frameborder="0" 
                    >
                </iframe>

                <p> <span style={{ fontWeight: '600' }}> {title} </span></p>
                {/* <p> { description} </p> */}
            </div>
            <div style={{
                width: '30%'
            }}>
                {/*  TODO: 사이드 리스트 화면을.. 위에서 youtubeData값을 매핑하여, Item컴포넌트에 적용한다. 굳이 List컴포넌트를 호출하여 다시 api통신을 할 필요가 없이... */}
                {/* <Item /> */}

                <ul className={styles.todo_ul}>
                    {
                        youtubeData.map((x) => 
                            <Item
                                key={kind === 'searchResult' ? x.id.videoId : x.id }
                                list={x}/>
                            //   <li key={x.id}> {x.id} </li>
                        )
                    }
                </ul>
                
            </div>
        </div>
            
        </>
        
    );
}; 

export default Detail;