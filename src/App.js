import './App.css';
import Navbar from './components/navbar'
import List from './components/list'
import Test from './test';
import { BrowserRouter, Route, Routes, useNavigate } from 'react-router-dom';
import { useEffect, useState } from 'react';
import Item from './components/item';
import Detail from './components/detail';
// import List from './components/list';

function App() {
  const [searchVal, setSearch] = useState('');
  const [youtubeData, setYoutubeData] = useState();
  // const navigate = useNavigate();

  // const [youtubeData, setYoutubeData] = useState();
  // let youtubeData = null;

  

  const onSearchVal = (e) => {
    console.log(`onSearchVal() value : ${e}`)
    setSearch(e);
    // navigate('/');
  };

  const handleYoutubeData = (val) => {
    console.log(`val : ${val}`); 
    
    setYoutubeData(val);
    // youtubeData = val;

  }
  console.log(`App() `);
  return (
    <>
      <div className='App'
        style={{
          // background: 'red'
          border: '1px solid black',
          width: '70%',
          display: 'block',
          // display: 'flex'
          // height: '80%',
        }}>
        
        {/* <Test/> */}

        
        {/* <Navbar
          onSearchVal={onSearchVal}/> */}
        {/* <List
          searchVal={searchVal}/> */}

        <BrowserRouter>
          <Navbar
            onSearchVal={onSearchVal}/>
          <Routes>
            <Route path='/' element={<List searchVal={searchVal} handleYoutubeData={handleYoutubeData} />}> </Route>
            <Route path='/components/detail' element={<Detail/>}></Route>
            <Route path='/components/detail/:videosId' element={<Detail youtubeData={youtubeData}/>}></Route>
          </Routes>
        </BrowserRouter>
      </div>

      
      
    </>
  );
}

export default App;
