import React from 'react';
import styles from './test.module.css';


const Test = () => {
    return (
        <>
        <div className={styles.flex_container}>
            <div className={styles.flex_item}></div>
            <div className={styles.flex_item}></div>
            <div className={styles.flex_item}></div>
            <div className={styles.flex_item}></div>
            <div className={styles.flex_item}></div>
            <div className={styles.flex_item}></div>
            <div className={styles.flex_item}></div>
            <div className={styles.flex_item}></div>
            <div className={styles.flex_item}></div>
            <div className={styles.flex_item}></div>
            <div className={styles.flex_item}></div>
            <div className={styles.flex_item}></div>
            <div className={styles.flex_item}></div>
            <div className={styles.flex_item}></div>
            <div className={styles.flex_item}></div>
            <div className={styles.flex_item}></div>
            <div className={styles.flex_item}></div>
            <div className={styles.flex_item}></div>
            <div className={styles.flex_item}></div>
            <div className={styles.flex_item}></div>
        </div>

        <div style={{ width: '100%'}}>
            <div style={{ width: '70%', float: 'left' }}>
                <iframe id="ytplayer" width="100%" height="360" style={{ padding: '1em' }}
                    src={`https://www.youtube.com/embed/Iiukq_ilT0Y?autoplay=1&origin=http://localhost:3000`}
                    frameborder="0" 
                    >
                </iframe>
                <p> <span style={{ fontWeight: '600' }}> tuitlee </span> </p>
            </div>
            <div style={{ display: '', width: '30%'}}>
                <ul style={{
                    display: 'flex',
                    flexWrap: 'wrap',
                    textAlign: 'center',
                    marginTop: '6px',
                    padding: '0'
                }}>
                    <li style={{
                        width: '100%',
                        listStyle: 'none',
                        padding: '1em',
                        cursor: 'pointer'
                    }}>
                        <div style={{ position: 'relative', boxShadow: '0 4px 8px 0 rgba(0,0,0,0.2)'}}>
                            <img
                                src='https://i.ytimg.com/vi/Jdvv5YNFEZ8/default.jpg' alt="Avatar" style={{ }} />
                            <span style={{position: 'absolute', }}> dasdfafafasfsfsa </span>
                        </div>
                    </li>
                    <li style={{
                        width: '100%',
                        listStyle: 'none',
                        padding: '1em',
                        cursor: 'pointer'
                    }}>
                        <div style={{ position: 'relative', boxShadow: '0 4px 8px 0 rgba(0,0,0,0.2)'}}>
                            <img
                                src='https://i.ytimg.com/vi/Jdvv5YNFEZ8/default.jpg' alt="Avatar" style={{ }} />
                            <span style={{position: 'absolute', }}> dasdfafafasfsfsa </span>
                        </div>
                    </li>
                    <li style={{
                        width: '100%',
                        listStyle: 'none',
                        padding: '1em',
                        cursor: 'pointer'
                    }}>
                        <div style={{ position: 'relative', boxShadow: '0 4px 8px 0 rgba(0,0,0,0.2)'}}>
                            <img
                                src='https://i.ytimg.com/vi/Jdvv5YNFEZ8/default.jpg' alt="Avatar" style={{ }} />
                            <span style={{position: 'absolute', }}> dasdfafafasfsfsa </span>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        
        

        <div style={{ position: 'relative'}}>
        <img
            src='https://i.ytimg.com/vi/Iiukq_ilT0Y/mqdefault.jpg' alt="Avatar" style={{ }} />
        <span style={{position: 'absolute'}}> Ep4 It‘s on you and I | BTS: Burn the Stage </span>
        </div>
        </>
        
    );
};

export default Test;